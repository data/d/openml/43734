# OpenML dataset: Nashville-Housing-Data

https://www.openml.org/d/43734

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This is home value data for the hot Nashville market.
Content
There are 56,000+ rows altogether.  However, I'm missing home detail data for about half. So if anyone wants to track that down then go for it! I'll be looking in the mean time. Enjoy.
Will add the Python file that retrieved this data once I clean it up.
Shameless plug:
visit this link for my latest project, a SQL magic function for IPython Notebook.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43734) of an [OpenML dataset](https://www.openml.org/d/43734). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43734/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43734/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43734/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

